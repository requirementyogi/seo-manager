package com.playsql.seo.macro;

import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.setup.sitemesh.SitemeshPageBodyRenderable;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.profiling.VelocitySitemeshPage;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.playsql.seo.Utils.firstNonNull;

public class VelocityContext {
    private final SettingsManager settingsManager;

    final static String BEGIN_TAG = "<!-- SEOManager:";
    final static String END_TAG = ":SEOManager -->";
    final static Pattern EACH_PROP = Pattern.compile("(.*?)=(.*)");

    public VelocityContext(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public String getTagsHtml(VelocitySitemeshPage page, Object body) {
        Map<String, String> meshProperties = page.getProperties();

        if (StringUtils.isBlank(meshProperties.get("page.pageId"))) {
            return confluenceBuiltInTitle(meshProperties);
        }

        Map<String, String> metadata = getMetadata(meshProperties, body);

        Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
        contextMap.put("metadata", metadata /* We shouldn't just write the metadata */);
        return VelocityUtils.getRenderedTemplate("metadata.vm", contextMap);
    }

    private String confluenceBuiltInTitle(Map<String, String> meshProperties) {

        Settings globalSettings = settingsManager.getGlobalSettings();
        String siteTitle = firstNonNull(globalSettings != null ? globalSettings.getSiteTitle() : "", "");
        String title = firstNonNull(meshProperties.get("title"), "");

        StringBuilder sb = new StringBuilder("<title>");
        sb.append(GeneralUtil.htmlEncode(title));
        String spaceName = meshProperties.get("page.spacename");
        if (StringUtils.isNotBlank(spaceName)) {
            sb.append(" - ").append(GeneralUtil.htmlEncode(spaceName));
        }
        sb.append(" - ").append(siteTitle).append("</title>");
        return sb.toString();
    }

    private Map<String, String> getMetadata(Map<String, String> meshProperties, Object body) {
        Map<String, String> metadata = new HashMap<>();
        Settings globalSettings = settingsManager.getGlobalSettings();
        metadata.put("siteTitle", globalSettings != null ? globalSettings.getSiteTitle() : "");
        metadata.put("title", meshProperties.get("title"));
        metadata.put("spaceName", meshProperties.get("page.spacename"));

        // Let all properties from the macro override ours if necessary
        if (body instanceof SitemeshPageBodyRenderable) {
            try {
                long start = System.nanoTime();
                StringWriter w = new StringWriter();
                ((SitemeshPageBodyRenderable) body).render(null, w);
                String bodyHtml = w.toString();

                String[] parts = bodyHtml.split(BEGIN_TAG);
                for (int i = 1; i < parts.length; i++) {
                    if (!parts[i].contains(END_TAG)) continue;
                    String part = parts[i].substring(0, parts[i].indexOf(END_TAG));

                    Matcher propMatcher = EACH_PROP.matcher(part);
                    while (propMatcher.find()) {
                        String value = GeneralUtil.unescapeEntities(propMatcher.group(2));
                        String prop = propMatcher.group(1);
                        metadata.put(prop, value);
                    }
                }
                long end = System.nanoTime();

                metadata.put("seo-rendertime", TimeUnit.MICROSECONDS.convert(end-start, TimeUnit.NANOSECONDS) + "us");
            } catch (RuntimeException re) {
                // Do nothing, say nothing
            } catch (IOException e) {
                // TODO remove
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        String title = metadata.get("title");
        String description = metadata.get("description");
        String spaceName = metadata.get("spaceName");
        String siteTitle = metadata.get("siteTitle");
        String composedSiteTitle = StringUtils.isBlank(spaceName) ? siteTitle : siteTitle + " - " + spaceName;
        String image = metadata.get("imagesrc");
        if (!metadata.containsKey("composedTitle")) {
            String composedTitle = title + " - " + composedSiteTitle;
            metadata.put("composedTitle", composedTitle);
        }

        if (!metadata.containsKey("twittertitle")) metadata.put("twittertitle", title);
        if (!metadata.containsKey("twitterdescription")) metadata.put("twitterdescription", description);
        if (!metadata.containsKey("twittertitle")) metadata.put("twittertitle", title);
        if (!metadata.containsKey("ogtitle")) metadata.put("ogtitle", title);
        if (!metadata.containsKey("ogtype")) metadata.put("ogtype", "article");
        //if (!metadata.containsKey("ogurl")) metadata.put("ogurl", "article");
        if (!metadata.containsKey("ogdescription")) metadata.put("ogdescription", description);
        if (!metadata.containsKey("ogsitename")) metadata.put("ogsitename", siteTitle);
        //if (!metadata.containsKey("ogpublishedtime")) metadata.put("ogpublishedtime", siteTitle);
        //if (!metadata.containsKey("ogmodifiedtime")) metadata.put("ogmodifiedtime", siteTitle);
        if (!metadata.containsKey("ogsection")) metadata.put("ogsection", spaceName);
        if (!metadata.containsKey("ogtag")) metadata.put("ogtag", metadata.get("keywords"));

        if (!metadata.containsKey("image")) metadata.put("image", image);
        if (!metadata.containsKey("twitterimage")) metadata.put("twitterimage", image);
        if (!metadata.containsKey("ogimage")) metadata.put("ogimage", image);

        return metadata;
    }

    private static Pattern ALL_HTML_TAGS = Pattern.compile("<.*?>");
    public static String convertToText(String body) {
        String bodyWithoutTags = ALL_HTML_TAGS.matcher(body).replaceAll("");
        String bodyWithoutSpaces = bodyWithoutTags.replaceAll("\\s+", " ").trim();
        String bodyWithoutHtmlEntities = GeneralUtil.unescapeEntities(bodyWithoutSpaces);
        return bodyWithoutHtmlEntities;
    }
}
