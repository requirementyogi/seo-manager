package com.playsql.seo;

public class Utils {
    public static <T> T firstNonNull(T... values) {
        if (values != null)
            for (T value : values)
                if (value != null) return value;
        return null;
    }
}
