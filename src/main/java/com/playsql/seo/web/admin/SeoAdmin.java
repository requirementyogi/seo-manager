package com.playsql.seo.web.admin;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.pages.*;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpacesQuery;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.extras.common.log.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

public class SeoAdmin extends ConfluenceActionSupport {

    private final static Logger.Log log = Logger.getInstance(SeoAdmin.class);

    private final static DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
    private final int TOMCAT_FORM_SIZE_LIMIT ;
    private final int TOMCAT_SAFE_LIMIT;
    private static final int SITEMAP_URL_LIMIT = 50000;

    private SpaceManager spaceManager;
    private PageManager pageManager;
    private ContentPermissionManager contentPermissionManager;
    private PermissionManager permissionManager;
    private SettingsManager settingsManager;

    private String robotstxt;
    private String sitemapxml;

    private File webappRoot;
    private File sitemapFile;
    private File robotsFile;
    private String robotsErrorMessage;
    private String sitemapErrorMessage;

    private List<PageDef> urlList = new ArrayList<>();

    {
        String tomcatRoot = System.getProperty("catalina.base");

        if (isWebappRoot(tomcatRoot)) {
            webappRoot = new File(tomcatRoot);
        } else if (isWebappRoot(tomcatRoot, "webapps")) {
            webappRoot = new File(tomcatRoot, "webapps");
        } else if (isWebappRoot(tomcatRoot, "webapps", "confluence")) {
            webappRoot = new File(new File(tomcatRoot, "webapps"), "confluence");
        } else if (isWebappRoot(tomcatRoot, "confluence")) {
            webappRoot = new File(tomcatRoot, "confluence");
        } else {
            webappRoot = new File(tomcatRoot, "install-dir");
        }

        String limit = System.getProperty("com.playsql.seo.limit.sitemap");
        if (StringUtils.isNumeric(limit)) {
            TOMCAT_FORM_SIZE_LIMIT = Integer.parseInt(limit, 10);
        } else {
            TOMCAT_FORM_SIZE_LIMIT = 1024 * 1024 * 14/10;
        }
        TOMCAT_SAFE_LIMIT = TOMCAT_FORM_SIZE_LIMIT * 3/4;
    }

    private static boolean isWebappRoot(String... subdirs) {
        File base = null;
        if (subdirs != null) for (String subdir : subdirs) {
            base = new File(base, subdir);
        }
        return new File(base, "login.vm").exists();
    }

    private File robots() {
        String presetLocation = System.getProperty("com.playsql.seo.locations.robots");
        if (presetLocation != null) {
            robotsFile = new File(presetLocation);
        } else {
            robotsFile = new File(webappRoot, "robots.txt");
        }
        return robotsFile;
    }

    private File sitemap() {
        String presetLocation = System.getProperty("com.playsql.seo.locations.sitemap");
        if (presetLocation != null) {
            sitemapFile = new File(presetLocation);
        } else {
            sitemapFile = new File(webappRoot, "sitemap.xml");
        }
        return sitemapFile;
    }

    public String doDefault() {
        File robotsFile = robots();
        File sitemapFile = sitemap();

        robotstxt = readFile(1, robotsFile);
        sitemapxml = readFile(2, sitemapFile);

        return INPUT;
    }

    private String readFile(int which, File robotsFile) {
        FileInputStream robotsContent = null;
        String result = null;
        String errorMessage = null;
        try {
            robotsContent = new FileInputStream(robotsFile);
            result = IOUtils.toString(robotsContent, StandardCharsets.UTF_8);
        } catch (FileNotFoundException e) {
            log.info("Error while getting the contents of " + robotsFile.getAbsolutePath(), e);
            errorMessage = " - Not Found";
        } catch (IOException e) {
            log.info("Error while getting the contents of " + robotsFile.getAbsolutePath(), e);
            errorMessage = " - Can't read - " + e.getMessage();
        } catch (RuntimeException e) {
            log.info("Error while getting the contents of " + robotsFile.getAbsolutePath(), e);
            errorMessage = " - Can't read - " + e.getMessage();
        } finally {
            IOUtils.closeQuietly(robotsContent);
        }
        switch (which) {
            case 1: robotsErrorMessage = errorMessage; break;
            case 2: sitemapErrorMessage = errorMessage; break;
        }
        return result;
    }

    public String doFill() {
        try {
            robots();
            sitemap();
            addActionMessage(getText("seo.admin.is.a.suggestion"));

            String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();

            if (!permissionManager.hasPermission(null, Permission.VIEW, PermissionManager.TARGET_APPLICATION)) {
                addActionError(getText("seo.admin.not.public"));
                return INPUT;
            }

            final SpacesQuery query = SpacesQuery.newQuery().withPermission(SpacePermission.VIEWSPACE_PERMISSION).forUser(null).build();
            final List<Space> allSpaces = spaceManager.getAllSpaces(query);
            for (Space space : allSpaces) {
                String spaceTitle = space.getDisplayTitle();
                if (urlList.size() < 50000) {
                    urlList.add(new PageDef(spaceTitle, "Space " + space.getKey() + " (" + space.getDisplayTitle() + ")"));

                    TreeBuilder treeBuilder = new TreeBuilder(null, contentPermissionManager, pageManager);
                    ContentTree tree = treeBuilder.createPageTree(space);
                    for (Page page : tree.getPages()) {
                        if (page.getTitle().startsWith("_"))
                            continue;
                        urlList.add(new PageDef(spaceTitle, page.getTitle(), baseUrl + page.getUrlPath(), page.getLastModificationDate(), "weekly", "1"));
                    }

                    final List recentlyAddedBlogPosts = pageManager.getRecentlyAddedBlogPosts(20, space.getKey());
                    for (Object object : recentlyAddedBlogPosts) {
                        if (!(object instanceof BlogPost)) continue;
                        BlogPost blog = (BlogPost) object;
                        urlList.add(new PageDef(spaceTitle, blog.getTitle(), baseUrl + blog.getUrlPath(), blog.getLastModificationDate(), "weekly", "1"));
                    }
                }

                urlList.add(new PageDef(spaceTitle, "URLs to browse the space (" + space.getDisplayTitle() + ")"));

                urlList.add(new PageDef(spaceTitle, "Recently updated", baseUrl + space.getBrowseUrlPath(), "", "weekly", "0.5"));
                urlList.add(new PageDef(spaceTitle, "Space root", baseUrl + space.getUrlPath(), "", "weekly", "0.3")); // Not really useful
            }

            urlList = limitSitemapSize(urlList);

            try {
                sitemapxml = renderTemplate("sitemapxml.vm", urlList);
                sitemapxml = limitFileSize(sitemapxml);
            } catch (OutOfMemoryError oome) {
                sitemapxml = ExceptionUtils.getStackTrace(oome);
                addActionError("The sitemap.xml couldn't be built. Too big. " + oome.getMessage());
            }

            Map<String, Object> contextMap2 = MacroUtils.defaultVelocityContext();
            robotstxt = VelocityUtils.getRenderedTemplate("robotstxt.vm", contextMap2);
            return INPUT;
        } catch (RuntimeException rex) {
            log.info("Error while filling the sitemap.", rex);
            addActionError("Error while filling the sitemap: " + rex.getMessage());
            return INPUT;
        }
    }

    /** This method is a hardcoded copy of VelocityUtils.getRenderedTemplate("sitemapxml.vm", contextMap),
     * because velocity tends to gag on big templates */
    private String renderTemplate(String template, List<PageDef> urlList) {
        // Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
        // contextMap.put("urlList", urlList);
        // return VelocityUtils.getRenderedTemplate("sitemapxml.vm", contextMap);
        StringBuilder sb = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
            "<urlset\n" +
            "    xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\"\n" +
            "    xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
            "    xsi:schemaLocation=\"http://www.sitemaps.org/schemas/sitemap/0.9\n" +
            "    http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd\">");
        for (PageDef pageDef : urlList) {
            if (pageDef.isComment()) {
                sb.append("\n\n<!-- ").append(GeneralUtil.htmlEncode(pageDef.getComment())).append(" -->");
            } else {
                sb.append("\n<url>");
                if (StringUtils.isNotBlank(pageDef.getComment()))
                    sb.append("<!-- ").append(GeneralUtil.htmlEncode(pageDef.getComment())).append(" -->");
                sb.append("\n\t<loc>").append(GeneralUtil.htmlEncode(pageDef.getUrl())).append("</loc>");
                if (StringUtils.isNotBlank(pageDef.getLastmod()))
                    sb.append("\n\t<lastmod>").append(GeneralUtil.htmlEncode(pageDef.getLastmod())).append("</lastmod>");
                if (StringUtils.isNotBlank(pageDef.getChangefreq()))
                    sb.append("\n\t<changefreq>").append(GeneralUtil.htmlEncode(pageDef.getChangefreq())).append("</changefreq>");
                if (StringUtils.isNotBlank(pageDef.getPriority()))
                    sb.append("\n\t<priority>").append(GeneralUtil.htmlEncode(pageDef.getPriority())).append("</priority>");
                sb.append("\n</url>");
            }
        }
        sb.append("\n</urlset>");
        return sb.toString();
    }


    private String limitFileSize(String sitemapxml) {
        if (sitemapxml.length() > TOMCAT_FORM_SIZE_LIMIT) {
            sitemapxml = Pattern.compile("\\s\\s+").matcher(sitemapxml).replaceAll(" ");
            sitemapxml = Pattern.compile("\\s*<!--.*?-->\\s*").matcher(sitemapxml).replaceAll(" ");
            if (sitemapxml.length() > TOMCAT_FORM_SIZE_LIMIT) {
                int length = sitemapxml.length();

                int position = sitemapxml.lastIndexOf("</url>", TOMCAT_SAFE_LIMIT);
                if (position != -1) {
                    sitemapxml = sitemapxml.substring(0, position) + "</url></urlset>";
                }
                int lengthAfter = sitemapxml.length();

                addActionError("The sitemap.xml was brutally truncated from " + FileUtils.byteCountToDisplaySize(length) + " to " + FileUtils.byteCountToDisplaySize(lengthAfter) + ", because Tomcat won't support any bigger size." +
                    " Despite our best efforts, it may even trigger a Java Heap Space/Thread deadlock when you submit this form: Be prepared to restart Confluence if this happens.");
                addActionError("Note: To remove this limit, change the max sitemap size with the system property -Dcom.playsql.seo.limit.sitemap=" + TOMCAT_FORM_SIZE_LIMIT + " (size in bytes)");
            }
        }
        return sitemapxml;
    }

    /** Sitemaps are officially limited to 50,000 URLs */
    private List<PageDef> limitSitemapSize(List<PageDef> urlList) {
        int exceedingCount = urlList.size() - SITEMAP_URL_LIMIT;
        int urlsRemoved = 0;
        if (exceedingCount > 0) {
            for (PageDef def : urlList) {
                if (def.getUrl() == null) {
                    exceedingCount--;
                    if (exceedingCount <= 0) {
                        break;
                    }
                }
            }
        }
        if (exceedingCount > 0) {
            Iterator<PageDef> it = urlList.iterator();
            while (it.hasNext() && exceedingCount > 0) {
                PageDef page = it.next();
                // Remove URLs to normal pages and space root, just keep the "recently updated" page
                if (Objects.equals(page.getComment(), "Space root")
                    || Objects.equals(page.getPriority(), "1")) {
                    it.remove();
                    exceedingCount--;
                    urlsRemoved++;
                }
            }
        }
        if (exceedingCount > 0) {
            urlList = urlList.subList(0, SITEMAP_URL_LIMIT);
            addActionError(exceedingCount + " pages removed from the sitemap.xml, because Google sets the limit to 50,000 URLs.");
        } else if (urlsRemoved > 0) {
            addActionError(urlsRemoved + " URLs were excluded from the sitemap.xml, because Google sets the limit to 50,000 URLs.");
        }
        return urlList;
    }

    public String doSave() {
        try {
            File robotsFile = robots();
            File sitemapFile = sitemap();

            if (StringUtils.isBlank(robotstxt)) {
                addActionError("The robots.txt is empty.");
                return INPUT;
            }

            if (StringUtils.isBlank(sitemapxml)) {
                addActionError("The sitemap.xml is empty.");
                return INPUT;
            }

            if (!write(robotsFile, robotstxt)) {
                return INPUT;
            }
            if (!write(sitemapFile, sitemapxml)) {
                return INPUT;
            }

            addActionMessage(getText("seo.admin.success"));
            return SUCCESS;
        } catch (RuntimeException rex) {
            log.info("Error while saving.", rex);
            addActionError("Error while saving: " + rex.getMessage());
            return INPUT;
        }
    }

    public String doLicense() {
        return "license";
    }

    private boolean write(File file, String contents) {
        PrintWriter writer = null;
        if (file == null)
            throw new RuntimeException("Attempt to write into a file that wasn't provided");
        if (StringUtils.isBlank(contents))
            throw new RuntimeException("Attempt to write into a file, but the contents weren't provided: " + file.getAbsolutePath());
        try {
            //file.getParentFile().mkdirs();
            file.createNewFile();
            writer = new PrintWriter(file, "UTF-8");
            writer.write(contents);
        } catch (IOException | RuntimeException e) {
            addActionError(getText("Exception while attempting to save: " + e.getMessage()));
            return false;
        } finally {
            IOUtils.closeQuietly(writer);
        }
        return true;
    }

    public String getRobotstxt() {
        return robotstxt;
    }

    public void setRobotstxt(String robotstxt) {
        this.robotstxt = robotstxt;
    }

    public String getSitemapxml() {
        return sitemapxml;
    }

    public void setSitemapxml(String sitemapxml) {
        this.sitemapxml = sitemapxml;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public void setContentPermissionManager(ContentPermissionManager contentPermissionManager) {
        this.contentPermissionManager = contentPermissionManager;
    }

    public void setPermissionManager(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    public class PageDef {
        private String url;
        private String lastmod;
        private String changefreq;
        private String priority;
        private String comment;
        private boolean isComment;
        private String spaceTitle;

        public PageDef(String spaceTitle, String comment) {
            this.spaceTitle = spaceTitle;
            this.comment = comment;
            this.isComment = true;
        }

        public PageDef(String spaceTitle, String comment, String url, String lastmod, String changefreq, String priority) {
            this.spaceTitle = spaceTitle;
            this.isComment = false;
            this.comment = comment;
            this.url = url;
            this.lastmod = lastmod;
            this.changefreq = changefreq;
            this.priority = priority;
        }

        public PageDef(String spaceTitle, String comment, String url, Date lastmod, String changefreq, String priority) {
            this.spaceTitle = spaceTitle;
            this.isComment = false;
            this.comment = comment;
            this.url = url;
            String dateAsText = df.format(lastmod);
            this.lastmod = dateAsText.substring(0, 22) + ":" + dateAsText.substring(22);
            this.changefreq = changefreq;
            this.priority = priority;
        }

        public PageDef() {

        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }

        public String getLastmod() {
            return lastmod;
        }

        public void setLastmod(String lastmod) {
            this.lastmod = lastmod;
        }

        public String getChangefreq() {
            return changefreq;
        }

        public void setChangefreq(String changefreq) {
            this.changefreq = changefreq;
        }

        public String getPriority() {
            return priority;
        }

        public void setPriority(String priority) {
            this.priority = priority;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public boolean isComment() {
            return isComment;
        }

        public String getSpaceTitle() {
            return spaceTitle;
        }

        public void setSpaceTitle(String spaceTitle) {
            this.spaceTitle = spaceTitle;
        }
    }

    public void setSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public List<PageDef> getUrlList() {
        return urlList;
    }

    public String getSitemapFilePathWithNonexistingPathHtml() {
        return highlightNonExistingPaths(sitemapFile, sitemapErrorMessage);
    }

    public String getRobotsFilePathWithNonexistingPathHtml() {
        return highlightNonExistingPaths(robotsFile, robotsErrorMessage);
    }

    private String highlightNonExistingPaths(File file, String errorMessage) {
        if (!file.exists()) {
            File parent = file.getParentFile();
            while (parent != null && !parent.exists()) {
                parent = parent.getParentFile();
            }
            if (parent != null) {
                // We've found the subpath that exists!
                String existingPath = GeneralUtil.htmlEncode(parent.getAbsolutePath());
                String path = GeneralUtil.htmlEncode(file.getAbsolutePath());
                String assemblyPath = existingPath
                    + "<span style=\"color: #ff5630;\">" + path.substring(existingPath.length()) + "</span>"
                    + (errorMessage != null ? GeneralUtil.htmlEncode(errorMessage) : "");
                return assemblyPath;
            }
        }
        return GeneralUtil.htmlEncode(file.getAbsolutePath() + (errorMessage != null ? errorMessage : ""));
    }
}
